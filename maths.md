standard syntax gitlab / preview WebIDE, does not work with live Preview VSCode

$`a^2+b^2=c^2`$

```math
a^2+b^2=c^2
```

$`Q`$

$`\mathbf{G}`$

new syntax [LaTeX-compatible fencing](https://docs.gitlab.com/ee/user/markdown.html#latex-compatible-fencing), works with live Preview VSCode

$a^2+b^2=c^2$

${a^2+b^2=c^2}$

$$
a^2+b^2=c^2
$$

$Q$

$\mathbf{G}$

